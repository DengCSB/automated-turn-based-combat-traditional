using System;

namespace GMPROG3Quiz1
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            // Test commit

			// Create archer
			Unit archer = new Unit();
			archer.Name = "Archer";
			archer.Hp = 100;
			archer.Mp = 50;
			archer.Power = 10;

			// Create skill for archer
			Strafe strafe = new Strafe();
			strafe.Name = "Strafe";
			strafe.MpCost = 5;
			strafe.Multiplier = 2.5f;

			// Add strafe to archer
			archer.Skills = new Skill[1];
			archer.Skills[0] = strafe;

			// Create monster
			Unit monster = new Unit();
			monster.Name = "Poring";
			monster.Hp = 30;
			monster.Mp = 10;
			monster.Power = 5;

			Unit winner = null;
			while(winner == null)
			{
				// Archer's move. Randomize archer's action
				Random r = new Random();
				int move = r.Next(1);
				if (move == 0)
				{
					archer.BasicAttack(monster);
				}
				else
				{
					archer.UseSkill("Strafe", monster);
				}
				Console.WriteLine(monster.Name + "'s HP: " + monster.Hp);

				// Check if monster is dead. If dead, determine the winner and exit out of the loop.
				if (!monster.Alive)
				{
					winner = archer;
					break;
				}

				// Monster's move
				monster.BasicAttack(archer);

				// Check if monster is dead. If dead, determine the winner and exit out of the loop.
				if (!archer.Alive)
				{
					winner = monster;
					break;
				}
				Console.WriteLine(archer.Name + "'s HP: " + archer.Hp);
			}

			Console.WriteLine(winner.Name + " is victorious!");

			Console.Read();
		}
	}
}
